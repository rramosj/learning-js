(() => {
  // Research DOM 

  // querySelector allow to get any element in the document
  const menu = document.querySelector('.menu');
  // arrays could have a mix of different types of values
  const list = [
    { label: 'Components', name: 'hola' },
    { label: 'Iconos' },
  ];

  let itemsHTML = '';

  // Access check
  list.forEach((item) => {
    const { label, name } = item;
    //let li = document.createElement('li');
    //li.innerHTML = label;
    //li.addEventListener('click', () => {
    //  console.log();
    //});
    //li.__data__ = item;
    //menu.appendChild(li);

    itemsHTML += `<li class="item" data-name="${name}">${label}</li>`;
  });

  menu.innerHTML += itemsHTML;
})();
